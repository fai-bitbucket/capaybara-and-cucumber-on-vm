Given /^I visit (.*)$/ do |path|
  visit path
end

Then /^I can see content (.*)$/ do |content|
  expect(page).to have_content(content)
end