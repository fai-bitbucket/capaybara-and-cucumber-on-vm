require 'capybara'
require 'capybara/cucumber'
require 'rspec/expectations'
require 'rest_client'
require 'pry'
require_relative 'cukes_helper'

Dir[File.expand_path('features/support/shared_helpers/**/*.rb')].each { |f| require f }

# The main setup for capyara configuration
# For selenium with chrome: if you'd like to force-upgrade to the latest version of chromedriver, run the script 'chromedriver-update'
# For selenium with firefox: download the geckodriver from https://github.com/mozilla/geckodriver/releases
CukesHelper.setup(capybara_driver: :poltergeist)
# Load shared features helper

World(FeaturesHelper)

# Wait for Ajax
World(WaitForAjax)

World(RSpec::Matchers)

Before do
  # do something here
end

After do |scenario|
  # do something here
end

After('@javascript') do
  wait_for_ajax
end

