module WaitForAjax
  def wait_for_ajax
    return unless js_test?
    Timeout.timeout(Capybara.default_max_wait_time) do
      loop until finished_all_ajax_requests?
    end
  end

  private
  def finished_all_ajax_requests?
    # See http://stackoverflow.com/questions/3148225/jquery-active-function [Fay 27.10.2016]
    result = page.evaluate_script('jQuery.active')
    result.present? && result.zero?
  end
end