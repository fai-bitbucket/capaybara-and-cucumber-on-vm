# -*- encoding : utf-8 -*-
require "ostruct"
require 'capybara-screenshot/cucumber'
require 'phantomjs'
require 'capybara-webkit'
require 'selenium-webdriver'
require 'capybara/poltergeist'

module CukesHelper
  DEFAULT_DRIVER            ||= :poltergeist
  WINDOW_SIZE               ||= [1920, 1080]
  SCREENSHOT_PATH           ||= "capybara-screenshots"
  CAPYBARA_WAIT_TIME        ||= 20
  POLTERGEIST_TIME_OUT      ||= 90
  DEFAULT_SELENIUM_BROWSER  ||= :firefox
  DEFAULT_PHANTOMJS_PATH    ||= Phantomjs.path
  APP_HOST                  ||= 'http://www.google.de'

  def self.config
    #The required options
    @@config ||= OpenStruct.new(
      on_ci: false,
      run_server: false,
      app_host: APP_HOST,
      capybara_driver: DEFAULT_DRIVER,
      phantomjs_path: DEFAULT_PHANTOMJS_PATH,
      browser: DEFAULT_SELENIUM_BROWSER,
      capybara_timeout: CAPYBARA_WAIT_TIME,
      poltergeist_timeout: POLTERGEIST_TIME_OUT,
    )
  end

  def self.setup(options={})
    cleanup
    set_config(options)
    register_drivers
    configure_capybara
    configure_capybara_screenshot
    print_settings
  end


  def self.info(message)
    @should_output ||= ARGV.any? ? true : false
    if @should_output
      puts "\033[33m#{message}\033[0m"
    end
  end

  def js_test?
    Capybara.current_driver == Capybara.javascript_driver
  end

  def self.run_with_parallel_tests?
    (env('TEST_ENV_NUMBER') || env('TEST_PROCESS_NUMBER')).present?
  end

  def self.set_config(options)
    #build ENV options
    env_options = {}
    config.marshal_dump.keys.each do |key|
      env_options[key] = env(key) unless env(key).nil?
    end

    options.merge(env_options).each do |key, value|
      if config.respond_to?(key)
        config.send("#{key}=", value)
      else
        info "I have no idea what to do with '#{key}: #{value}'! So, i will just ignore it!"
      end
    end

  end

  def self.configure_capybara
    Capybara.app_host = config.app_host
    Capybara.run_server = config.run_server
    Capybara.default_max_wait_time = config.capybara_timeout.to_i
    # Capybara.ignore_hidden_elements = false
    Capybara.default_driver = config.capybara_driver.to_sym
    Capybara.javascript_driver = config.capybara_driver.to_sym
  end

  def self.register_drivers
    register_poltergeist_driver
    register_webkit_driver
    register_selenium_driver
  end

  def self.register_webkit_driver
    Capybara.register_driver :webkit do |app|
      Capybara::Webkit::Driver.new(app).tap do |driver|
        driver.browser.ignore_ssl_errors
        driver.resize_window(WINDOW_SIZE[0], WINDOW_SIZE[1])
      end
    end
  end

  def self.register_selenium_driver
    options = {
                browser: config.browser.to_sym,
                #resynchronize: true
              }

    Capybara.register_driver :selenium do |app|
      Capybara::Selenium::Driver.new(app, options)
    end

  end

  def self.register_poltergeist_driver
    silent_out = STDOUT.dup
    silent_out.instance_eval { def write(*_); end }

    loud_out = STDOUT.dup
    loud_out.instance_eval { def write(args); super("[POLTERGEIST DEBUG] #{args}\n") unless args == "\n"; end }

    options = {
      js_errors: false, # Javascript errors get re-raised in ruby
      window_size: WINDOW_SIZE,
      phantomjs_options: ['--ignore-ssl-errors=true', '--load-images=no', '--disk-cache=false'],
      phantomjs_logger: silent_out,
      timeout: config.poltergeist_timeout,
      phantomjs: config.phantomjs_path
    }

    Capybara.register_driver :poltergeist do |app|
      Capybara::Poltergeist::Driver.new(app, options)
    end

    # Use this if you want to report JS errors
    Capybara.register_driver :poltergeist_jserrors do |app|
      Capybara::Poltergeist::Driver.new(app,
                                        options.merge({
                                          js_errors: true
                                        })
      )
    end

    # Use this driver if you want to see every command/response from the phantomjs server
    Capybara.register_driver :poltergeist_debug do |app|
      Capybara::Poltergeist::Driver.new(app,
                                        options.merge({
                                          debug: true,
                                          logger: loud_out
                                        })
      )
    end
  end

  def self.env(name, &block)
    val = ENV[name.to_s]
    yield val if val && block
    val
  end

  def self.configure_capybara_screenshot
    Capybara::Screenshot.prune_strategy = :keep_last_run
    Capybara::Screenshot.append_timestamp = false
    Capybara::Screenshot.register_filename_prefix_formatter(:cucumber) do |scenario|
      "#{SCREENSHOT_PATH}/#{scenario.location.file.gsub('/', '_')}:#{scenario.location.line}"
    end
  end

  def self.cleanup
    FileUtils.rm_rf(File.expand_path(Capybara.save_path.to_s, SCREENSHOT_PATH))
  end

  def self.print_settings
    info "Use capybara_driver: #{config.capybara_driver} #{capybara_driver_emoji}"
    info "Use capybara max wait time: #{config.capybara_timeout}"
    info "Use app host: #{Capybara.app_host}"

    if selenium?
      info "Use browser: #{config.browser}"
    end

    if poltergeist?
      info "Use poltergeist timeout: #{config.poltergeist_timeout}"
    end

  end

  def self.capybara_driver_emoji
    case config.capybara_driver.to_sym
      when :selenium
        "*📺 *"
      when :poltergeist, :poltergeist_jserrors, :poltergeist_debug
        "*👻 *"
      when :webkit
        "*📠 *"
    end
   end

  def self.selenium?
    !config.capybara_driver.to_s.match(/selenium/).nil?
  end

  def self.poltergeist?
    !config.capybara_driver.to_s.match(/poltergeist/).nil?
  end

  def self.webkit?
    !config.capybara_driver.to_s.match(/webkit/).nil?
  end

end
