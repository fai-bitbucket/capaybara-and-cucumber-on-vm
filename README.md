### What is this repository for? ###

This repo shows a simple setup for capybara with cucumber.
To save the entire installation hassle, vagrant and virtuallbox is used. 
The goal is to start the VM over vagrant and execute the Cukes directly on it.

### How do I get set up? ###	
 - install Vagrant https://www.vagrantup.com/	
 - install Virtualbox https://www.virtualbox.org/
 - clone the repo git: git clone https://fifio@bitbucket.org/fifio/capaybara-and-cucumber-on-vm.git
 - in shell: cd capaybara-and-cucumber-on-vm
 - vagrant up
 - ...and let the cukes run: vagrant ssh -c run_cuke
 
 